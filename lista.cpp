#include <iostream>

using namespace std;

struct Talia
{
	//int id;
	char kolor;
	//char znak;
	int wartosc;

};
struct WezelTalia
{
	Talia karta;
	WezelTalia * Next;
	WezelTalia * Prev;

};
WezelTalia * Head = NULL;
WezelTalia * Tail = NULL;
WezelTalia * Current = NULL;

WezelTalia * TworzGlowe(char kolor, int wartosc)
{
	//Nie uzywac
	WezelTalia *result = new WezelTalia();
	result->karta.kolor = kolor;
	result->karta.wartosc = wartosc;

	result->Next = nullptr;
	return result;

}
void WstawNaPoczatek(WezelTalia * &Head, char kolor, int wartosc)
{
	WezelTalia * nowy = new WezelTalia();
	nowy->karta.kolor = kolor;
	nowy->karta.wartosc = wartosc;

	if (Head == nullptr)
	{
		nowy->Next = nullptr;
		nowy->Prev = nullptr;
		Tail = nowy;

	}
	else
	{
		Head->Prev = nowy;
		nowy->Next = Head;
	}
	Head = nowy;

}
void WstawNaKoniec(WezelTalia * &Tail, char kolor, int wartosc)
{
	WezelTalia * nowy = new WezelTalia();
	nowy->karta.kolor = kolor;
	nowy->karta.wartosc = wartosc;

	Tail->Next = nowy;
	nowy->Prev = Tail;
	Tail = nowy;
}
void WstawWezel(WezelTalia * prev, char kolor, int wartosc)
{
	WezelTalia * nowy = new WezelTalia();
	nowy->karta.kolor = kolor;
	nowy->karta.wartosc = wartosc;

	nowy->Next = prev->Next;
	prev->Next = nowy;
}
void UsunWezel(WezelTalia * Head, WezelTalia * doUsuniecia)
{
	WezelTalia * prev = Head;

	while (prev->Next != doUsuniecia)
	{
		prev = prev->Next;
	}
	prev->Next = doUsuniecia->Next;

	delete doUsuniecia;
}
void Kasuj(WezelTalia * &Head)
{
	WezelTalia * Current = Head;
	WezelTalia * doKasacji = Head;

	while (Current != nullptr)
	{
		doKasacji = Current;
		Current = Current->Next;

		delete doKasacji;
	}
	Head = nullptr;
}
void WyswietlKarte(const Talia &o)
{
	cout << o.kolor << "   " << o.wartosc << endl;
}
void WyswietlTalie(WezelTalia*	const Head)
{
	WezelTalia * Current = Head;
	while (Current != nullptr)
	{
		WyswietlKarte(Current->karta);
		Current = Current->Next;
	}
}
int main()
{


	WstawNaPoczatek(Head, 'w', 2);
	WstawNaPoczatek(Head, 'e', 3);
	WstawNaPoczatek(Head, 'r', 4);
	WstawNaKoniec(Tail, 'd', 9);
	WstawNaKoniec(Tail, 'g', 8);


	WyswietlTalie(Head);
	Kasuj(Head);
	WyswietlTalie(Head);
	return 0;
}